package json;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class AllTheCards {

// Jan 2020-25 Saturday make changes form Branch1 home 
    public static void main(String[] args) {
        String[] Suit = {"DIAMONDS", "CLUBS", "HEARTS", "SPADES"};
        String[] Rank = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};

        JSONArray cards = new JSONArray();

        List<String> suit1 = new ArrayList<String>();
       // List<String> rank1 = new ArrayList<String>();
        // Shuffle the Suit first
        // Shuffle the elements in the array
        List<String> ShuffledSuit1 = Arrays.asList(Suit);
        List<String> ShuffledRank1 = Arrays.asList(Rank); // define this also here used later in the loop

        System.out.println(ShuffledSuit1);
        Collections.shuffle(ShuffledSuit1);
        System.out.println(ShuffledSuit1);
        Suit = (String[]) ShuffledSuit1.toArray();
        System.out.println("Suit.length=" + Suit.length + " Rank.length=" + Rank.length);
        for (int i = 0; i < Suit.length; i++) {
            suit1.add(Suit[i]);
            Collections.shuffle(ShuffledRank1);
            ;
            Rank = (String[]) ShuffledRank1.toArray();
            for (int j = 0; j < Rank.length; j++) {
               // rank1.add(Suit[i] + " " + Rank[j]);
                cards.put(getCard(Suit[i], Rank[j]));
            }

        }
        System.out.printf("suit1=%s%n", suit1);
// jan 2020-25 small cosmetic change
        System.out.println("JSON Array Cards are =" + cards);
        JSONArray cards2 =  cards;
        System.out.println("JSON Array Cards2 =" + cards2);

        ArrayList<String> list = new ArrayList<String>();

        if (cards != null) {
            int len = cards.length();
            for (int i=0;i<len;i++){
                list.add(cards.get(i).toString());
            }
        }
        System.out.println( "list=" + list);
        Collections.shuffle(list);
        System.out.println( "list size=" + list.size() + " list=" + list);

    }
    static JSONObject getCard(String suit, String value){
        JSONObject card = new JSONObject();
        card.put("suit", suit);
        card.put("value", value);
        return card;
    }
}
