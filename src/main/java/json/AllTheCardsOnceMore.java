package json;

/*
is the same as AllTheCard now 12/11/2019
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class AllTheCardsOnceMore {

    public static void main(String[] args) {
        String[] Suit = {"DIAMONDS", "CLUBS", "HEARTS", "SPADES"};
        String[] Rank = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};

        JSONArray cards = new JSONArray();
        JSONArray cards2 = new JSONArray();
        List<String> suit1 = new ArrayList<String>();

        List<String> ShuffledSuit1 = Arrays.asList(Suit);
        List<String> ShuffledRank1 = Arrays.asList(Rank); // define this also here used later in the loop

        System.out.println(ShuffledSuit1);
        Collections.shuffle(ShuffledSuit1);
        System.out.println(ShuffledSuit1);
        Suit = (String[]) ShuffledSuit1.toArray();
        System.out.println("Suit.length=" + Suit.length + " Rank.length=" + Rank.length);
        for (int i = 0; i < Suit.length; i++) {
            suit1.add(Suit[i]);
            Collections.shuffle(ShuffledRank1);
            ;
            Rank = (String[]) ShuffledRank1.toArray();
            for (int j = 0; j < Rank.length; j++) {
                  cards.put(getCard(Suit[i], Rank[j]));
            }

        }
        System.out.printf("suit1=%s%n", suit1);
        System.out.println("JSON Array Cards=" + cards);


       // JSONArray to ArrayList
        ArrayList<String> arraylist1 = new ArrayList<String>();

        if (cards != null) {
            int len = cards.length();
            for (int i=0;i<len;i++){
                arraylist1.add(cards.get(i).toString());
            }
        }

        //System.out.println( "list=" + arraylist1);
        Collections.shuffle(arraylist1);
        System.out.println( "list size=" + arraylist1.size() + " list=" + arraylist1);
      // now convert Arraylist to JSONArray

         cards2=getJSONArrayFromList(arraylist1);
         System.out.println("cards2 length=" + cards2.length()  + cards2);

    }

    /**
     * Converts an Arraylist to JSONArray
     * @param l1
     * @return j1
     */
   static JSONArray getJSONArrayFromList (ArrayList l1){
        JSONArray j1 = new JSONArray();
        String s1, s2;
        for (int j = 0; j < l1.size();  j++) {
            s1 = l1.get(j).toString();
            s2 = s1.substring(s1.indexOf(":\"") + 2, s1.indexOf(",\"value\"") - 1);
            String s3 = s1.substring(s1.indexOf("value\":\"") + 8, s1.indexOf("\"}"));
            j1.put(getCard(s2, s3));
        }
        return j1;
    }
    static JSONObject getCard(String suit, String value){
        JSONObject card = new JSONObject();
        card.put("suit", suit);
        card.put("value", value);
        return card;
    }

}
