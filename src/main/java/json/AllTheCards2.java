package json;

/*
author : Gopi
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import org.json.JSONArray;
import org.json.JSONObject;
 // Simple cdode to produce a random deck of cards 12.11.2019
public class AllTheCards2 {

    public static void main(String[] args) {
        String[] Suit = {"DIAMONDS", "CLUBS", "HEARTS", "SPADES"};
        String[] Rank = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};

        JSONArray cards = new JSONArray();
        JSONArray cards2 = new JSONArray();

         for (int i = 0; i < Suit.length; i++) {
            for (int j = 0; j < Rank.length; j++) {
                 cards.put(getCard(Suit[i], Rank[j]));
            }
        }

        System.out.println("JSON Array Cards=" + cards);
        ArrayList<String> list = new ArrayList<String>();
        if (cards != null) {
            int len = cards.length();
            for (int i=0;i<len;i++){
                list.add(cards.get(i).toString());
            }
        }
        System.out.println( "list=" + list);
        Collections.shuffle(list);
        System.out.println( "list size=" + list.size() + " list=" + list);

        String[] array1 = list.toArray(new String[list.size()]);
        System.out.println( " new array1=" + Arrays.toString(array1));


            for (int i = 0; i < array1.length; i++) {
                cards2.put(array1[i]);
            }

        System.out.println( "array1=" + array1);

    }
    static JSONObject getCard(String suit, String value){
        JSONObject card = new JSONObject();
        card.put("suit", suit);
        card.put("value", value);
        return card;
    }
}
